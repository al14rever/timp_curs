using Microsoft.VisualStudio.TestTools.UnitTesting;
using irc_client;
using System.Threading;
using System.Net.Sockets;
using System.Text;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            string login = System.DateTime.Now.ToString();
            login = login.Replace(" ", "");
            login = login.Replace(".", "");
            login = login.Replace(":", "");
            login += "AA";
            IRC_Connection irc = new IRC_Connection("127.0.0.1", "6667", login, login, login);

            irc.Connect();

            Thread.Sleep(500);
            if (!irc.isConnected())
            {
                Assert.Fail();
                return;
            }

            irc.SendCommand("/list");

            string message = "";
            byte[] buffer = new byte[1];

            Thread.Sleep(500);

            NetworkStream stream = irc.getClient().GetStream();

            while (stream.DataAvailable)
            {
                try
                {
                    stream.Read(buffer, 0, 1);
                }
                catch
                {
                    Assert.Fail();
                    return;
                }

                message += Encoding.ASCII.GetString(buffer, 0, 1);
                if (message.Contains("\r\n") || message.Length > 1024)
                {
                    message = message.Replace("\r\n", "");
                    break;
                }
            }

            if (message == "")
            {
                Assert.Fail();
                return;
            }

            IRC_Reply rp = new IRC_Reply(message);

            if (rp.getParameters() != login)
            {
                Assert.Fail();
                return;
            }

            Assert.IsTrue(true);
        }
    }
}
