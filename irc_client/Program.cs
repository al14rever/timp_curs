﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace irc_client
{
    public class IRC_Reply
    {
	    private string message;
        private string username;
        private string prefix;
        private string command;
        private int numericReply;
        private string parameters;
        private string trailing;
        private bool hasTrailing;
        private bool hasUser;
        private bool isNumericReply;

	    public IRC_Reply(string message)
        {
            int indx;
            List<string> p;
            List<string> pk;

            this.numericReply = 0;
            this.isNumericReply = false;
            this.hasTrailing = false;
            this.hasUser = false;

            this.message = message;

            //Get Prefix
            indx = 0;
            p = message.Split(' ').ToList();
            if (indx < p.Count)
            {
                p[indx] = p[indx].Trim(':');
                this.prefix = p[indx];

                if (p[indx].Contains('!'))
                {
                    pk = p[indx].Split('!').ToList();
                    if (pk[0] != null)
                    {
                        this.username = pk[0];
                        this.hasUser = true;
                    }
                }
            }

            //Get command
            indx++;
            if (indx < p.Count)
            {
                p[indx] = p[indx].Trim(':');
                this.command = p[indx];

                this.isNumericReply = true;
                foreach(char ch in p[indx])
                {
                    if(!Char.IsNumber(ch))
                    {
                        this.isNumericReply = false;
                        break;
                    }
                }

                if (this.isNumericReply)
                {
                    this.numericReply = Convert.ToInt32(p[indx]);
                }
            }

            //Get parameters
            indx++;
            if (indx < p.Count)
            {
                p[indx] = p[indx].Trim(':');
                this.parameters = p[indx];
            }

            //Get trailing
            indx++;
            if (indx < p.Count)
            {
                p[indx] = p[indx].Trim(':');
                hasTrailing = true;

                if(p.Count() > indx) {
                    for (int i = indx; i < p.Count; i++)
                    {
                        this.trailing += p[i];
                        if (i != (p.Count() - 1))
                        {
                            this.trailing += " ";
                        }
                    }
                } 
                else
                {
                    this.trailing += p[indx];
                }
            }
        }

        public string getMessage() 
        {
            return message;
        }

        public string getPrefix()
        {
            return prefix; 
        }

        public string getCommand() 
        {
            return command;
        }

        public string getParameters() 
        {
            return parameters;
        }

        public string getTrailing() 
        {
            if (hasTrailing)
            {
                return trailing;
            }
            else
            {
                return parameters;
            }
        }

        public string getUsername() 
        {
            if (hasUser)
            {
                return username;
            }
            else
            {
                return prefix;
            }
        }

        public int getNumericReply() 
        {
            if (isNumericReply)
            {
                return numericReply;
            }
            else
            {
                return -1;
            }
        }

        public bool getHasTrailing() 
        {
            return hasTrailing;
        }

    };

    public class IRC_Thread
    {
        private TcpClient client = null;
        private NetworkStream stream = null;

        private string channel = "";

        public IRC_Thread(TcpClient client, string channel)
        {
            this.client = client;
            this.stream = this.client.GetStream();
            this.channel = channel;

            byte[] data = System.Text.Encoding.ASCII.GetBytes(string.Format("JOIN {0}\r\n", this.channel));
            try
            {
                this.stream.Write(data, 0, data.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine("Something went wrong!\n Error: " + e);
            }
        }

        public void SendMessage(string message)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(string.Format("PRIVMSG {0} :{1}\r\n", this.channel, message));
            try
            {
                this.stream.Write(data, 0, data.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine("Something went wrong!\n Error: " + e);
            }
        }

        public string getChannel() 
        {
            return channel;
        }
    }

    public class IRC_Connection
    {
        private TcpClient client = null;
        private NetworkStream stream = null;

        private string ip_address = "";
        private string username = "";
        private string nickname = "";
        private string fullname = "";
        private int port;

        IRC_Thread channel = null;

        private bool conected = false;

        public IRC_Connection(string ip_address, string port, string nickname, string username, string fullname)
        {
            this.ip_address = ip_address;
            this.port = Convert.ToInt32(port);
            this.nickname = nickname;
            this.username = username;
            this.fullname = fullname;
        }

        public void Connect()
        {
            try
            {
                this.client = new TcpClient(this.ip_address, this.port);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open socket!\n Error: " + e);
                this.conected = false;
                return;
            }

            this.stream = this.client.GetStream();
            this.conected = true;

            Console.WriteLine("Succesfully connected to {0} on port {1}!\n", this.ip_address, this.port);

            Byte[] data = null;

            data = System.Text.Encoding.ASCII.GetBytes(string.Format("NICK {0}\r\n", this.nickname));
            this.stream.Write(data, 0, data.Length);
            Thread.Sleep(500);

            data = System.Text.Encoding.ASCII.GetBytes(string.Format("USER {0} 0 * :{1}\r\n", this.username, this.fullname));
            this.stream.Write(data, 0, data.Length);
            Thread.Sleep(500);

            string message = "";
            byte[] buffer = new byte[1];
            IRC_Reply rpl = null;

            while (true)
            {
                message = "";
                while (stream.DataAvailable)
                {
                    try
                    {
                        stream.Read(buffer, 0, 1);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Something went wrong!\n Error: " + e);
                        this.conected = false;
                        return;
                    }

                    message += Encoding.ASCII.GetString(buffer, 0, 1);
                    if (message.Contains("\r\n") || message.Length > 1024)
                    {
                        message = message.Replace("\r\n", "");
                        break;
                    }
                }
                rpl = new IRC_Reply(message);
                if (rpl.getNumericReply() == 433)
                {
                    Console.WriteLine(rpl.getTrailing());
                    Console.WriteLine("Disconect from server.");
                    Exit(null);
                    return;
                } 
                else if (rpl.getNumericReply() == 1)
                {
                    Console.WriteLine(rpl.getTrailing());
                    break;
                }
                else if (rpl.getPrefix() != null)
                {
                    if (rpl.getPrefix().Contains("PING"))
                    {
                        data = System.Text.Encoding.ASCII.GetBytes(string.Format("PONG :{0}\r\n", rpl.getCommand()));
                        stream.Write(data, 0, data.Length);
                    }
                }
            }
        }

        public void StartReplying()
        {
            Thread ReplyThread = new Thread(this.ReadServer);
            ReplyThread.Start();
        }

        public void SendCommand(string command)
        {
            if(command == "")
            {
                return;
            }
            if(command[0] == '/')
            {
                List<string> p;
                p = command.Split(' ').ToList();
                if (p[0] == "/join" && p.Count > 1)
                {
                    this.channel = new IRC_Thread(this.client, p[1]);
                }
                else
                {
                    command = command.Trim('/');
                    byte[] data = System.Text.Encoding.ASCII.GetBytes(string.Format("{0}\r\n", command));
                    try
                    {
                        this.stream.Write(data, 0, data.Length);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Something went wrong!\n Error: " + e);
                    }
                }
            }
            else
            {
                if (this.channel == null)
                {
                    Console.WriteLine("You are not in a channel!");
                    return;
                }
                this.channel.SendMessage(command);
            }
        }

        public bool isConnected()
        {
            return conected;
        }

        public void Exit(string message)
        {
            byte[] data = System.Text.Encoding.ASCII.GetBytes(string.Format("QUIT :{0}\r\n", message));
            try
            {
                this.stream.Write(data, 0, data.Length);
            }
            catch (Exception e)
            {
                Console.WriteLine("Something went wrong!\n Error: " + e);
            }
            this.stream.Close();
            this.client.Close();
            this.conected = false;
        }

        public TcpClient getClient()
        {
            return this.client;
        }

        private void ReadServer()
        {
            IRC_Reply reply = null;

            int numeric;

            string message = "";
            byte[] buffer = new byte[1];

            while (true)
            {
                message = "";
                while (stream.DataAvailable)
                {
                    try
                    {
                        stream.Read(buffer, 0, 1);
                    }
                    catch
                    {
                        Console.WriteLine("Socket disconnected!");
                        this.conected = false;
                        return;
                    }

                    message += Encoding.ASCII.GetString(buffer, 0, 1);
                    if (message.Contains("\r\n") || message.Length > 1024)
                    {
                        message = message.Replace("\r\n", "");
                        
                        break;
                    }
                }

                reply = new IRC_Reply(message);
                numeric = reply.getNumericReply();

                if (reply.getCommand() != null && numeric < 0)
                {
                    if (reply.getCommand().Contains("privmsg") || reply.getCommand().Contains("PRIVMSG"))
                    {
                        Console.WriteLine("{0}: {1}", reply.getUsername(), reply.getTrailing());
                    }
                    else if (reply.getCommand().Contains("topic") || reply.getCommand().Contains("TOPIC"))
                    {
                        Console.WriteLine("{0} changed topic to {1}", reply.getUsername(), reply.getTrailing());
                    }
                    else if (reply.getCommand().Contains("join") || reply.getCommand().Contains("JOIN"))
                    {
                        Console.WriteLine("{0} joined channel {1}", reply.getUsername(), reply.getParameters());
                    }
                    else if (reply.getCommand().Contains("quit") || reply.getCommand().Contains("QUIT"))
                    {
                        Console.WriteLine("{0} disconected: {1}", reply.getUsername(), reply.getTrailing());
                    }
                }
                else if (reply.getPrefix().Contains("ERROR") || reply.getPrefix().Contains("error"))
                {
                    Console.WriteLine("ERROR");
                }
                else if (reply.getPrefix().Contains("PING") || reply.getPrefix().Contains("ping"))
                {
                    Console.WriteLine("Sending keep alive.");
                    byte[] data = System.Text.Encoding.ASCII.GetBytes(string.Format("PONG :{0}\r\n", reply.getCommand()));
                    stream.Write(data, 0, data.Length);
                }
                else if ((numeric >= 1 && numeric <= 5) || (numeric >= 370 && numeric <= 380) || (numeric >= 250 && numeric <= 270) || (numeric == 322))
                {
                    Console.WriteLine(reply.getTrailing());
                } 
                else if (reply.getTrailing() != null)
                {
                    Console.WriteLine(reply.getTrailing());
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            IRC_Connection irc = null;
            string message;

            if (args.Length < 5)
            {
                Console.WriteLine("Invalid usage: irc_client [ip address] [port] [nickname] [username] [realname]");
                return;
            }

            irc = new IRC_Connection(args[0], args[1], args[2], args[3], args[4]);

            irc.Connect();
            irc.StartReplying();

            while (irc.isConnected())
            {
                message = Console.ReadLine();
                message = message.Replace("\r", "");
                message = message.Replace("\n", "");
                irc.SendCommand(message);
            }
        }
    }
}
