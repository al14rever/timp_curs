﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace irc_server
{
    class IRC_Reply
    {
        private string message = "";
        private string username = "";
        private string prefix = "";
        private string command = "";
        private int numericReply;
        private string parameters = "";
        private string trailing = "";
        private bool hasTrailing;
        private bool hasUser;
        private bool isNumericReply;

        public IRC_Reply(string message)
        {
            int indx;
            List<string> p;
            List<string> pk;

            this.numericReply = 0;
            this.isNumericReply = false;
            this.hasTrailing = false;
            this.hasUser = false;

            this.message = message;

            //Get Prefix
            indx = 0;
            p = message.Split(' ').ToList();
            if (indx < p.Count)
            {
                p[indx] = p[indx].Trim(':');
                this.prefix = p[indx];

                if (p[indx].Contains('!'))
                {
                    pk = p[indx].Split('!').ToList();
                    if (pk[0] != null)
                    {
                        this.username = pk[0];
                        this.hasUser = true;
                    }
                }
            }

            //Get command
            indx++;
            if (indx < p.Count)
            {
                p[indx] = p[indx].Trim(':');
                this.command = p[indx];

                this.isNumericReply = true;
                foreach (char ch in p[indx])
                {
                    if (!Char.IsNumber(ch))
                    {
                        this.isNumericReply = false;
                        break;
                    }
                }

                if (this.isNumericReply)
                {
                    this.numericReply = Convert.ToInt32(p[indx]);
                }
            }

            //Get parameters
            indx++;
            if (indx < p.Count)
            {
                p[indx] = p[indx].Trim(':');
                this.parameters = p[indx];
            }

            //Get trailing
            indx++;
            if (indx < p.Count)
            {
                p[indx] = p[indx].Trim(':');
                hasTrailing = true;

                if (p.Count() > indx)
                {
                    for (int i = indx; i < p.Count; i++)
                    {
                        this.trailing += p[i];
                        if (i != (p.Count() - 1))
                        {
                            this.trailing += " ";
                        }
                    }
                }
                else
                {
                    this.trailing += p[indx];
                }
            }
        }

        public string getMessage()
        {
            return message;
        }

        public string getPrefix()
        {
            return prefix;
        }

        public string getCommand()
        {
            return command;
        }

        public string getParameters()
        {
            return parameters;
        }

        public string getTrailing()
        {
            if (hasTrailing)
            {
                return trailing;
            }
            else
            {
                return parameters;
            }
        }

        public string getUsername()
        {
            if (hasUser)
            {
                return username;
            }
            else
            {
                return prefix;
            }
        }

        public int getNumericReply()
        {
            if (isNumericReply)
            {
                return numericReply;
            }
            else
            {
                return -1;
            }
        }

        public bool getHasTrailing()
        {
            return hasTrailing;
        }

    };

    class IRC_Channel
    {
        private string name;
        private List<string> users = new List<string>();

        public IRC_Channel(string name)
        {
            this.name = name;
        }

        public void joinUser(string name, string username)
        {
            this.users.Add(name);
            byte[] data = null;
            foreach (string user in this.users)
            {
                data = System.Text.Encoding.ASCII.GetBytes(string.Format(":{0}!{1}@{2} JOIN :{3}\r\n", name, username, IRC_Connection.getHostname(), this.name));
                try
                {
                    IRC_User usr = IRC_Connection.getUser(user);
                    if (usr != null)
                    {
                        usr.getClient().GetStream().Write(data, 0, data.Length);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Something went wrong!\n Error: " + e);
                }      
            }
            
        }

        public string getName()
        {
            return name;
        }

        public string[] getUsersList()
        {
            return users.ToArray();
        }
    }

    static class IRC_Connection
    {
        private static List<IRC_Channel> channels = new List<IRC_Channel>();
        private static List<IRC_User> users = new List<IRC_User>();
        private static string motd = "";
        private static string hostname = "NONE";
        private static TcpListener Listener;

        public static void setMotd(string motd)
        {
            IRC_Connection.motd = motd;
        }

        public static void Init(string hostname, string port)
        {
            IPAddress ip = IPAddress.Parse(IPAddress.Any.ToString());
            IRC_Connection.Listener = new TcpListener(ip, Convert.ToInt32(port));
            IRC_Connection.Listener.Start();
            IRC_Connection.hostname = hostname;

            while (true)
            {
                TcpClient Clnt = Listener.AcceptTcpClient();

                IRC_Connection.users.Add(new IRC_User(Clnt));
                Thread thrd = new Thread(IRC_Connection.users[IRC_Connection.users.Count-1].ClientThread);
                thrd.Start();
            }

        }

        public static string getHostname()
        {
            return IRC_Connection.hostname;
        }

        public static IRC_User getUser(string name)
        {
            foreach(IRC_User user in IRC_Connection.users)
            {
                if (user.getNickname() == name)
                {
                    return user;
                }
            }
            return null;
        }

        public static IRC_User[] getUsersList()
        {
            return IRC_Connection.users.ToArray();
        }

        public static IRC_Channel[] GetChannelsList()
        {
            return IRC_Connection.channels.ToArray();
        }

        public static IRC_Channel GetChannel(string name)
        {
            if (name[0] != '#') 
            {
                return null;
            }
            foreach (IRC_Channel chan in IRC_Connection.channels)
            {
                if (chan.getName() == name)
                {
                    return chan;
                }
            }
            IRC_Connection.channels.Add(new IRC_Channel(name));
            return IRC_Connection.channels[IRC_Connection.channels.Count - 1];
        }
    }

    class IRC_User
    {
        private string nickname = "";
        private string username = "";
        private string fullname = "";
        private string ip_adr = "";
        private string channel = "";

        private bool connected = false;

        private TcpClient client = null;
        private NetworkStream stream = null;

        public IRC_User(TcpClient client)
        {
            this.client = client;
            this.stream = this.client.GetStream();
            this.ip_adr = (this.client.Client.LocalEndPoint as IPEndPoint).Address.ToString();
            Console.WriteLine("Info: Connected " + this.ip_adr);
        }

        public TcpClient getClient()
        {
            return this.client;
        }

        public string getNickname()
        {
            return this.nickname;
        }

        public void Close()
        {
            this.connected = false;
            this.stream.Close();
            this.client.Close();
            Console.WriteLine("Info: Closed {0}",  this.ip_adr);
        }

        public void Welcome()
        {
            IRC_User[] users = IRC_Connection.getUsersList();
            int cnt = 0;
            foreach (IRC_User user in users)
            {
                if (this.nickname == user.getNickname())
                {
                    if (cnt == 1)
                    {
                        this.Send(string.Format(":{0} 433 * {1} :Nickname is already in use.", IRC_Connection.getHostname(), this.nickname));
                        this.nickname = "";
                        return;
                    } 
                    else
                    {
                        cnt++;
                    }
                    
                }
            }
            this.Send(string.Format(":{0} 001 * {1} :Welcome to the Internet Relay Network {1}!{2}@{0} ", IRC_Connection.getHostname(), this.nickname, this.username));
            this.connected = true;
        }

        private void Send(string message)
        {
            Byte[] data = null;

            data = System.Text.Encoding.ASCII.GetBytes(string.Format("{0}\r\n", message));
            try
            {
                this.stream.Write(data, 0, data.Length);
            }
            catch
            {
                Console.WriteLine("ERROR: Socket write error!");
                this.Close();
            }
        }

        private bool SendPrivMsg(IRC_Reply rep)
        {
            foreach (IRC_User user in IRC_Connection.getUsersList())
            {
                if (user.getNickname() == rep.getCommand())
                {
                    byte[] data = System.Text.Encoding.ASCII.GetBytes(string.Format(":{0}!{5}@{1} {4} {2} :{3}\r\n", this.nickname, IRC_Connection.getHostname(), rep.getCommand(), rep.getParameters(), rep.getPrefix(), this.username));
                    try
                    {
                        user.getClient().GetStream().Write(data, 0, data.Length);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Something went wrong!\n Error: " + e);
                    }
                    return true;
                }
            }

            IRC_Channel chan = IRC_Connection.GetChannel(rep.getCommand());
            if (chan != null)
            {
                if (chan.getName() == rep.getCommand())
                {
                    byte[] data = null;
                    foreach (string user in chan.getUsersList()) 
                    {
                        if(user != this.username)
                        {
                            data = System.Text.Encoding.ASCII.GetBytes(string.Format(":{0}!{5}@{1} {4} {2} :{3}\r\n", this.nickname, IRC_Connection.getHostname(), rep.getCommand(), rep.getParameters(), rep.getPrefix(), this.username));
                            try
                            {
                                IRC_Connection.getUser(user).getClient().GetStream().Write(data, 0, data.Length);
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Something went wrong!\n Error: " + e);
                            }
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        private void List()
        {
            this.Send(string.Format(":{0} 321 {1}  Channel :Users  Name", IRC_Connection.getHostname(), this.username));
            foreach (IRC_Channel chan in IRC_Connection.GetChannelsList())
            {
                this.Send(string.Format(":{0} 322 {1} {2} {3} : ", IRC_Connection.getHostname(), this.username, chan.getName(), chan.getUsersList().Length));
            }
            this.Send(string.Format(":{0} 323 {1} :End of /LIST", IRC_Connection.getHostname(), this.username));
        }

        private void Names(string name)
        {
            IRC_Channel chan = IRC_Connection.GetChannel(name);
            if (chan == null)
            {
                this.Send("ERROR");
            }
            string users = "";
            foreach (string user in chan.getUsersList())
            {
                users += (user + " ");
            }
            this.Send(string.Format(":{0} 353 {1} = {2} :{3}", IRC_Connection.getHostname(), this.nickname, name, users));
            this.Send(string.Format(":{0} 353 {1} {2} :End of /NAMES list.", IRC_Connection.getHostname(), this.nickname, name));
        }

        public void ClientThread()
        {
            IRC_Reply reply = null;

            int numeric;

            string message = "";
            byte[] buffer = new byte[1];

            while (true)
            {
                message = "";
                while (stream.DataAvailable)
                {
                    try
                    {
                        stream.Read(buffer, 0, 1);
                    }
                    catch
                    {
                        Close();
                        return;
                    }

                    message += Encoding.ASCII.GetString(buffer, 0, 1);
                    if (message.Contains("\r\n") || message.Length > 1024)
                    {
                        message = message.Replace("\r\n", "");
                        break;
                    }
                }

                reply = new IRC_Reply(message);
                numeric = reply.getNumericReply();

                if (reply.getPrefix().Contains("PRIVMSG") || reply.getPrefix().Contains("privmsg"))
                {
                    if (!this.SendPrivMsg(reply))
                    {
                        this.Send("ERROR");
                    }
                } 
                else if (reply.getPrefix().Contains("QUIT") || reply.getPrefix().Contains("quit")) {
                    if (!this.SendPrivMsg(reply))
                    {
                        this.Send("ERROR");
                    }
                }
                else if (reply.getPrefix().Contains("JOIN") || reply.getPrefix().Contains("join"))
                {
                    IRC_Channel chan = IRC_Connection.GetChannel(reply.getCommand());
                    if(chan != null)
                    {
                        chan.joinUser(this.nickname, this.username);
                        this.Names(chan.getName());
                    }
                    else
                    {
                        this.Send("ERROR");
                    }
                    
                    
                }
                else if (reply.getPrefix().Contains("PING") || reply.getPrefix().Contains("ping"))
                {
                    this.Send(string.Format("PONG :{0}", reply.getCommand()));
                }
                else if (reply.getPrefix().Contains("LIST") || reply.getPrefix().Contains("list"))
                {
                    this.List();

                }
                else if (reply.getPrefix().Contains("NAMES") || reply.getPrefix().Contains("names"))
                {
                    this.Names(reply.getCommand());
                }
                else if (reply.getPrefix().Contains("USER") || reply.getPrefix().Contains("user"))
                {
                    this.username = reply.getCommand();
                    this.fullname = reply.getTrailing().Split(':').Last<string>().Trim(' ');
                    if (this.nickname != "")
                    {
                        this.Welcome();
                    }
                }
                else if (reply.getPrefix().Contains("NICK") || reply.getPrefix().Contains("nick"))
                {
                    this.nickname = reply.getCommand();
                    if (this.username != "" && this.fullname != "")
                    {
                        this.Welcome();
                    }
                }
                else if (message != "")
                {
                    this.Send("ERROR");
                }
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length < 2)
            {
                Console.WriteLine("Invalid usage: irc_server [host name] [port]");
                return;
            }
            Console.WriteLine("Starting server...");
            IRC_Connection.Init(args[0], args[1]);
        }
    }
}
